set -e

# Criar arquivo para chave ssh
mkdir -p ~/.ssh
touch ~/.ssh/id_rsa

echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa


# Desabilita confirmação no terminal ao conectar
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config