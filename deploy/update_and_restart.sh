set -e

# Deleta o repositório com o código antigo
rm -rf /home/ubuntu/node-js-starter/

# Clona o repositório de novo
git clone https://gitlab.com/Doctor-28/node-js-starter


source /home/ubuntu/.nvm/nvm.sh
npm i pm2 -g

# Encerra a API qu estava rodando
echo "Reiniciando API.."
pm2 kill

cd /home/ubuntu/node-js-starter

# Instala as dependências
npm install


# Inicia a API novamente
pm2 start npm -- start

pm2 status