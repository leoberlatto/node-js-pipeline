echo "Starting deploy..."

set -e

# Setando a chave privada da nossa instância
chmod 001 ./deploy/set_ssh_key.sh
./deploy/set_ssh_key.sh

# Usando o ip na variável que definimos no Gitlab
DEPLOY_SERVER=$DEPLOY_SERVER


echo "deploying to ${DEPLOY_SERVER}"
ssh ubuntu@${DEPLOY_SERVER} 'bash' < ./deploy/update_and_restart.sh
