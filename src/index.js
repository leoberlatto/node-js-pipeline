const express = require('express')
const app = express()
const port = 3000

const dotenv = require('dotenv')

app.get('/', (req, res) => {
    dotenv.config({ path: process.env.NODE_ENV === 'development' ? '.env.dev' : '.env' })

    res.send({ teste: true})
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})